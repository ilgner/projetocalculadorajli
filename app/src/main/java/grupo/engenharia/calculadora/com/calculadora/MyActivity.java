package grupo.engenharia.calculadora.com.calculadora;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;


public class MyActivity extends ActionBarActivity {
    TextView textView;
    Button btSoma;
    Button btSubtracao;
    Button btDivisao;
    Button btMultiplicacao;
    EditText n1;
    EditText n2;
    EditText resultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        inicia();
    }

    private void inicia() {
        btSoma= (Button) findViewById(R.id.button);

        btSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(n1.getText().length() == 0 || n2.getText().length() == 0 ) {
                    Toast.makeText(MyActivity.this, "Digite os dois numeros", Toast.LENGTH_LONG).show();
                }else {
                    double num1 =Double.parseDouble(n1.getText().toString());
                    double num2 = Double.parseDouble(n2.getText().toString());
                    resultado.setText((num1 + num2) + "");
                }
            }
        });
        btSubtracao=(Button) findViewById(R.id.button2);
        btSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(n1.getText().length() == 0 || n2.getText().length() == 0 ) {
                    Toast.makeText(MyActivity.this, "Digite os dois numeros", Toast.LENGTH_LONG).show();
                }else {
                    double num1 =Double.parseDouble(n1.getText().toString());
                    double num2 = Double.parseDouble(n2.getText().toString());
                    resultado.setText(num1 - num2 + "");
                }
            }
        });
        btDivisao= (Button) findViewById(R.id.button3);
        btDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(n1.getText().length() == 0 || n2.getText().length() == 0 ) {
                    Toast.makeText(MyActivity.this, "Digite os dois numeros", Toast.LENGTH_LONG).show();
                }else{
                    double num1 =Double.parseDouble(n1.getText().toString());
                    double num2 = Double.parseDouble(n2.getText().toString());

                    if (num2 != 0) {
                        double r = num1 / num2;
                        DecimalFormat df = new DecimalFormat("0.0000000000");
                        resultado.setText(df.format(r) + "");
                    } else {
                        Toast.makeText(MyActivity.this, "Segundo numero não pode ser 0", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        btMultiplicacao= (Button) findViewById(R.id.button4);
        btMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(n1.getText().length() == 0 || n2.getText().length() == 0 ) {
                    Toast.makeText(MyActivity.this, "Digite os dois numeros", Toast.LENGTH_LONG).show();
                }else {
                    double num1 =Double.parseDouble(n1.getText().toString());
                    double num2 = Double.parseDouble(n2.getText().toString());
                    resultado.setText((num1 * num2) + "");
                }
            }
        });
        n1 = (EditText) findViewById(R.id.editText);
        n2= (EditText) findViewById(R.id.editText2);
        resultado= (EditText) findViewById(R.id.editText3);
    }

    private int incrementa(int num) {
        return ++num;
    }

    private int decrementa(int num) {
        return --num;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }
}
